package font.raul.TennisKata;

import java.util.ArrayList;

public class Team {

    private String firstPlayer;
    private String secondPlayer;
    private int points = 0;
    private int games = 0;
    private int sets = 0;
    private ArrayList<String> team = new ArrayList<>();

    public Team() {
        this.firstPlayer = "one player";
        this.team.add(firstPlayer);
    }

    public int hasManyPlayers() {
        return this.team.size();
    }

    public void hasTwoPlayers() {
        this.secondPlayer = "other player";
        team.add(this.secondPlayer);
    }

    public int getPoints() {
        return points;
    }

    public int getGames() {
        return games;
    }

    public int getSets() {
        return sets;
    }
}
