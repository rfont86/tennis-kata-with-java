package font.raul.TennisKata;

import java.util.ArrayList;

public class Court {

    private Team spanishTeam;
    private Team swissTeam;
    private ArrayList<Team> teams = new ArrayList<Team>();

    public Court() {
        this.spanishTeam = new Team();
        this.swissTeam = new Team();
        this.teams.add(spanishTeam);
        this.teams.add(swissTeam);
    }

    public void twoPlayersByTeam() {
        this.spanishTeam.hasTwoPlayers();
        this.swissTeam.hasTwoPlayers();
    }

    public int hasManyTeams() {
        return this.teams.size();
    }

    public int playersInSpain() {
        return this.spanishTeam.hasManyPlayers();
    }

    public int playersInSwitzerland() {
        return this.swissTeam.hasManyPlayers();
    }

}
