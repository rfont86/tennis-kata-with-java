package font.raul.TennisKata;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class CourtTest {

    @Test
    public void there_are_two_teams() {
        Court court = new Court();
        int teamsInCourt = 2;

        int result = court.hasManyTeams();

        assertEquals(teamsInCourt, result);
    }

    @Test
    public void each_team_has_one_player() {
        Court court = new Court();
        int spainOnePlayer = 1;
        int switzerlandOnePlayer = 1;

        int resultSpain = court.playersInSpain();
        int resultSwitzerland = court.playersInSwitzerland();

        assertEquals(spainOnePlayer, resultSpain);
        assertEquals(switzerlandOnePlayer, resultSwitzerland);
    }

    @Test
    public void each_team_has_two_player() {
        Court court = new Court();
        int spainOnePlayer = 2;
        int switzerlandOnePlayer = 2;

        court.twoPlayersByTeam();

        int resultSpain = court.playersInSpain();
        int resultSwitzerland = court.playersInSwitzerland();

        assertEquals(spainOnePlayer, resultSpain);
        assertEquals(switzerlandOnePlayer, resultSwitzerland);
    }

}