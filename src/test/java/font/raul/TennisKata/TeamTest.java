package font.raul.TennisKata;

import org.junit.Test;

import static org.junit.Assert.*;

public class TeamTest {

    @Test
    public void team_has_one_member() {
        Team team = new Team();
        int players = 1;

        int result = team.hasManyPlayers();

        assertEquals(players, result);
    }

    @Test
    public void team_has_two_members() {
        Team team = new Team();
        int players = 2;

        team.hasTwoPlayers();
        int result = team.hasManyPlayers();

        assertEquals(players, result);
    }

    @Test
    public void a_team_starts_with_zero_points() {
        Team team = new Team();
        int points = 0;
        int games = 0;
        int sets = 0;

        int pointsResult = team.getPoints();
        int gamesResult = team.getGames();
        int setsResult = team.getSets();

        assertEquals(points, pointsResult);
        assertEquals(games, gamesResult);
        assertEquals(sets, setsResult);
    }
}